RxJqueryExtension
=================
工具背景说明：
这个库是在处理SQL可视化生成的过程中萌生的想法。
之前去看过老外的
https://github.com/Reactive-Extensions/RxJS
还有一个是bacon什么的
这么几个库，然后决定自己写一个。
原因：
1、老外的库抽象太多，当你真的想组合使用观察者的时候，很难找到正确的API组合方式。
2、老外的库API设计的不够“实用”。我需要的就是简单的观察-通知，但是他没有一个API能够
简单明了的做这么件事情--PS：可能我没找到，因为当时时间紧迫。
3、觉得除了jquery以外，再加上一个复杂的库会很累赘。
结果：
1、我写了这么个库
2、基于这个库衍生了一个插件--专门处理级联select的插件
3、写了2个例子，一个基于quint对主库做单元测试的，一个是
test网页对衍生插件做测试的。

API说明
我的API非常简单，
当一个jq对象--由于产生背景-dom元素被限制成了input和select以及button
$(selector).reactive*() -- *表示select和input
这个jq对象就被注册给了一个闭包托管的通知中心--类似iOS的notification center，特别说明一点通知中心的通知变更都是key-value，key是
dom对象的name属性，value就是变化后的值
当有jq对象调用
$(selector).connectRx(map)--这个时候就会根据map中的参数说明来观察已经注册在通知中心中的对象的name

小例子片段：
$('#select2').reactiveSelect().connectRx({
    targets:['select1'],func:function(data){
        console.log(data['select1']);
    }
});

$('#select1').reactiveSelect().connectRx({
    targets:['select2'],func:function(data){
        console.log(data['select2']);
    }
});

上面的话就是将select1和select2都注册到通知中心，
并且互相观察对方的值变化